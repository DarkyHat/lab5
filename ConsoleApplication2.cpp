#include "pch.h"
#include <locale.h>
#include <fstream>
#include <string>
#include <iostream>
#include <cstdio>

using namespace std;

int main() {
	setlocale(0, "rus");
	int snt = 0;
	string filename; 
	printf("Введите имя исходного файла в формате имя файла.txt\n");
	cin >> filename;
	ifstream file; 
	file.open("C:\\Users\\" + filename); 
	if (!file)
	{
		cout << "Файл не открыт\n\n";
		exit(1);
	}
	else
	{
		cout << "Файл открыт!\n\n";
	}
	int k = 0;
	file.seekg(0, std::ios::end);
	k = file.tellg();
	file.close();
	file.open("C:\\Users\\" + filename);
	printf("Введите 1, если хотите разделить файл на заданную длину. Введите 2, если хотите разделить файл на равные файлы\n");
	int o = 0, i = 0;
	scanf_s("%d", &o);
	string name, number = "1"; 
	printf("Введите имя будущих файлов\n");
	cin >> name;
	if (o == 1) {
		cout << "Введите размер фрагментов, на которые нужно разбить файл\n" << endl;
		cin >> i;
		i++;
		int s = 0;
		char *buff = new char[i];
		int cnt = 0;
		while (s < (k/(i-1)) + 1) {
			file.get(buff, i);
			if (!file.eof() || buff[0] != 0) {
				string address = (string)("C:\\Users\\" + name + "_" + number + ".txt");
				ofstream file2(address); 
				while (cnt < i - 1) { 
					file2 << buff[cnt];
					cnt++;
				}
				file2.close(); 
				cnt = 0;
				number = to_string(stoul(number) + 1); 
				while (cnt < i) { 
					buff[cnt] = 0;
					cnt++;
				}
				cnt = 0;
				s++;
			}
		}
		file.close();
	}
	if (o == 2) {
		printf("Введите на сколько файлов нужно разделить исходный файл\n");
		int i = 0;
		int cnt = 0, l = 0;
		scanf_s("%d", &l);
		if ((k/2) <= l || (k%l) == 0) {
			i = (int)(k / l) + 1;
		}
		else {
			i = (int)(k / l) + 2;
		}
		char *buff = new char[i];
		int s = 0;
		while (s < l - 1) {
			file.get(buff, i); 
			if (!file.eof() || buff[0] != 0) {
				string address = (string)("C:\\Users\\" + name + "_" + number + ".txt");
				ofstream file2(address); 
				while (cnt < i - 1) { 
					file2 << buff[cnt];
					cnt++;
				}
				file2.close(); 
				cnt = 0;
				number = to_string(stoul(number) + 1);
				while (cnt < i) { 
					buff[cnt] = 0;
					cnt++;
				}
				cnt = 0;
				s++;
			}
		}
		s = 0;
		string address = (string)("C:\\Users\\" + name + "_" + number + ".txt");
		ofstream file2(address);
		while (s < (k-((l-1)*(i-1)))) {
			file.get(buff, 2);
			file2 << buff[0];
			buff[0] = 0;
			s++;
		}
		file2.close();
		file.close();
	}
	return 0;
}